<!DOCTYPE html>
<html lang="en">
    <head>

        <title>@yield('title', 'Homepage') - Sissy Boutique Residence Budapest</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>

        {{-- Main header --}}
        <div id="main-header">

            {{-- Top bar --}}
            @include('common.top-bar')
            {{-- /Top bar --}}

            @section('main_header_inner')
            @show
        </div>
        {{-- /Main header --}}

        {{-- Main content --}}
        <div id="main-content">
            @section('main_content')
            @show
        </div>
        {{-- /Main content --}}

        {{-- Main footer --}}
        <div id="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col justify-content-center">
                        @include('common.footer-nav')
                    </div>
                </div>
            </div>
        </div>
        {{-- /Main footer --}}

        @section('js_body')
            <script src="/js/app.js"></script>
        @show
    </body>
</html>