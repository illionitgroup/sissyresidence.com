<h2>New contact request received</h2>
<p><b>Name: </b> {{ isset($data['first_name']) ? $data['first_name'] : '' }} {{ isset($data['last_name']) ? $data['last_name'] : '' }}</p>
<p><b>Email: </b> {{ isset($data['email']) ? $data['email'] : '' }}</p>
<p><b>Phone: </b> {{ isset($data['phone']) ? $data['phone'] : '' }}</p>
<p><b>Comment: </b> {{ isset($data['comment']) ? $data['comment'] : '' }}</p>