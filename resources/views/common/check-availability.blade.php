<div id="check-availability-wrapper" class="d-none d-lg-block">
    <div id="check-availability" class="container">
        <div class="row align-items-center">
            <div class="col-md-4 title">
                <h2>Book now</h2>
            </div>
            <div class="col-md-8 form">

                <!--Booking Engine Start-->
                <script data-cfasync="false" src="https://ibe.sabeeapp.com/bewidget.php?id=2b275ebf2699f1e0e0504ede9b1616fd" type="text/javascript"></script>
                <!--Booking Engine End-->
            </div>
        </div>
    </div>
</div>