<div id="main-nav-wrapper" class="d-none d-lg-block">
    <div id="main-nav" class="container">
        <div class="row justify-content-center">
            <div class="col-5">
                <nav class="nav nav-inline nav-fill">
                    <a class="nav-link active" href="/#about">About</a>
                    <a class="nav-link" href="/#rooms">Rooms</a>
                    <a class="nav-link disabled" href="/#contact">Contact</a>
                    <a class="nav-link disabled" href="{{ route('privacy_policy') }}">Privacy Policy</a>
                </nav>
            </div>
        </div>
    </div>
</div>