<div id="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-4 phone align-self-center">
                <a href="tel:+3612345678">+36 1 234 5678</a>
            </div>
            <div class="col-md-4 logo">
                <a href="{{ route('homepage') }}">
                    <img src="/images/general/logo.png" alt="Sissy Boutique Residence - Budapest - Hungary">
                </a>
            </div>
            <div class="col-md-4 book-now-button">
                <script src="https://ibe.sabeeapp.com/bewidget.php?id=7640a4be3c4679f06c16d030514c56ff" type="text/javascript"></script>
            </div>
        </div>
    </div>
</div>