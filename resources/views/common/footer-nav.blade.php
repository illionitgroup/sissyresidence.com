<ul id="footer-nav" clasS="list-unstyled list-inline">
    <li class="list-inline-item">
        <a class="nav-link active" href="/#about">About</a>
    </li>
    <li class="list-inline-item">
        <a class="nav-link" href="/#rooms">Rooms</a>
    </li>
    <li class="list-inline-item">
        <a class="nav-link disabled" href="/#contact">Contact</a>
    </li >
    <li class="list-inline-item">
        <a class="nav-link disabled" href="{{ route('privacy_policy') }}">Privacy Policy</a>
    </li>
</ul>