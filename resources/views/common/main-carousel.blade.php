<div id="main-carousel-wrapper" class="container-fluid">
    <div class="row">
        <div class="col">
            <div id="main-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#main-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#main-carousel" data-slide-to="1"></li>
                    <li data-target="#main-carousel" data-slide-to="2"></li>
                    <li data-target="#main-carousel" data-slide-to="3"></li>
                    <li data-target="#main-carousel" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img src="/images/main-carousel/01.jpg" alt="01">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/main-carousel/02.jpg" alt="02">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/main-carousel/03.jpg" alt="03">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/main-carousel/04.jpg" alt="04">
                    </div>
                    <div class="carousel-item">
                        <img src="/images/main-carousel/05.jpg" alt="05">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#main-carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#main-carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    {{-- Main nav --}}
    @include('common.main-nav')
    {{-- /Main nav --}}

    {{-- Check availability --}}
    @include('common.check-availability')
    {{-- /Check availability --}}
</div>
