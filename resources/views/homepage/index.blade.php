@extends('layouts.base')

@section('main_header_inner')

    {{-- Main carousel --}}
    @include('common.main-carousel')
    {{-- /Main carousel --}}
@endsection

@section('main_content')

    {{-- Rooms --}}
    <div id="rooms" class="container main-content-block">
        <h2>Rooms</h2>
        <div class="row">

            {{-- Room box --}}
            <div class="col-md-4">
                <div class="room-box">
                    <div class="image">
                        <img src="/images/rooms/junior-suite.jpg" alt="Junior suite">
                    </div>
                    <div class="content">
                        <h3 class="text-center">Junior suite</h3>

                        <div class="row">
                            <div class="col"></div>
                            <div class="col">
                                <div class="booking-button">
                                    <script src="https://ibe.sabeeapp.com/bewidget.php?id=cee07fff0043f91c1b76e2987c2e0dca" type="text/javascript"></script>
                                </div>
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Room box --}}

            {{-- Room box --}}
            <div class="col-md-4">
                <div class="room-box">
                    <div class="image">
                        <img src="/images/rooms/double-room.jpg" alt="Double Room">
                    </div>
                    <div class="content">
                        <h3 class="text-center">Double Room</h3>

                        <div class="row">
                            <div class="col"></div>
                            <div class="col">
                                <div class="booking-button">
                                    <script src="https://ibe.sabeeapp.com/bewidget.php?id=3f7f6cdf9ab19e617fc7a580b26f61d1" type="text/javascript"></script>
                                </div>
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Room box --}}

            {{-- Room box --}}
            <div class="col-md-4">
                <div class="room-box">
                    <div class="image">
                        <img src="/images/rooms/deluxe-double-room.jpg" alt="Deluxe Double Room">
                    </div>
                    <div class="content">
                        <h3 class="text-center">Deluxe Double Room</h3>

                        <div class="row">
                            <div class="col"></div>
                            <div class="col">
                                <div class="booking-button">
                                    <script src="https://ibe.sabeeapp.com/bewidget.php?id=170377f75433bcb41ca9160dd07364ea" type="text/javascript"></script>
                                </div>
                            </div>
                            <div class="col"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Room box --}}
        </div>
    </div>
    {{-- /Rooms --}}

    {{-- Icons --}}
    <div id="icons" class="main-content-block d-none d-lg-block">
        <img src="/images/general/icons.png" alt="Services">
    </div>
    {{-- /Icons --}}

    {{-- About --}}
    <div id="about" class="container main-content-block">
        <h2>Sissy Residence is a great accommodation in Budapest</h2>
        <div class="row">
            <div class="col text">
                <p>This accommodation Featuring free WiFi, Sissy Residence is situated in Budapest, 800 m from Dohany Street Synagogue.</p>
                <p>The rooms are fitted with a small TV. Some units feature a seating area for your convenience. Each room comes with a private bathroom. For your comfort, you will find free toiletries and a hairdryer.</p>
                <p>There is luggage storage space at the property.</p>
                <p>State Opera House is 900 m from Sissy Residence, while Hungarian National Museum is 1.1 km away. The airport is Budapest Liszt Ferenc Airport, 16 km from Sissy Residence. We can handle private airport shuttle service.</p>
                <p>Erzsébetváros is a great choice of Budapest for travellers interested in nightlife, health spas and food. Perfect accommodation.</p>
            </div>
        </div>
    </div>
    {{-- /About --}}


    {{-- Contact --}}
    <div id="contact" class="container main-content-block">
        <h2>Contact</h2>
        <p>Thanks for your interest. Please complete the form below to send us your question or comment and we'll get back to you as soon as possible!</p>
        <div class="row">
            <div class="col-md-6">

                @if(Cache::get('contact_sent') == true)
                    <div class="alert alert-success">
                        <p><b>Your request has been sent successfully!</b></p>
                    </div>
                    <?php \Cache::forget('contact_sent') ?>
                @endif

                {!! Form::open(['url' => route('send_contact_form')]) !!}
                <div class="form-group">
                    <div class="row">

                        {{-- First name --}}
                        <div class="col-md-6">
                            {!! Form::label('first_name', 'First name') !!}
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => true]) !!}
                        </div>
                        {{-- /First name --}}

                        {{-- Last name --}}
                        <div class="col-md-6">
                            {!! Form::label('last_name', 'Last name') !!}
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => true]) !!}
                        </div>
                        {{-- /Last name --}}

                    </div>
                </div>
                <div class="form-group">
                    <div class="row">

                        {{-- Email --}}
                        <div class="col-md-6">
                            {!! Form::label('email', 'Email address') !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'required' => true]) !!}
                        </div>
                        {{-- /Email --}}

                        {{-- Phone --}}
                        <div class="col-md-6">
                            {!! Form::label('phone', 'Phone') !!}
                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                        </div>
                        {{-- /Phone --}}
                    </div>
                </div>
                <div class="form-group">
                    {{-- Comment --}}
                    {!! Form::label('comment', 'Comment') !!}
                    {!! Form::textarea('comment', null, ['class' => 'form-control', 'rows' => 4]) !!}
                    {{-- /Comment --}}
                </div>

                <div class="form-group">
                    <p><small>By submitting this contact form, I acknowledge that I may be entered into email campaigns from Sissy Residence.</small></p>
                    <p>
                        {!! Form::submit('Send', ['name' => 'submit_contact', 'class' => 'btn']) !!}
                    </p>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="col-md-6 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2695.489582193487!2d19.067445791823108!3d47.499855965741524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc6641b7e741%3A0xf54d980c7717d8b9!2sBudapest%2C+Erzs%C3%A9bet+krt.+24%2C+1073!5e0!3m2!1shu!2shu!4v1517729024440" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
    {{-- /Contact --}}

@endsection

@section('js_body')
    @parent

@endsection