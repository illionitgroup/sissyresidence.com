@extends('layouts.base')

@section('title', 'Privacy Policy')

@section('main_content')

    {{-- Privacy policy --}}
    <div id="privacy-policy" class="main-content-block container">
        <div class="row">
            <div class="col">
                <h2>Privacy Statement</h2>
                <p>Effective Date: December 2017</p>
                <p>This privacy statement applies to&nbsp;<a href="http://www.sissyresidence.com/" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://www.sissyresidence.com&amp;source=gmail&amp;ust=1518251651060000&amp;usg=AFQjCNGDyjt0mtuYNU2ze2mr3Ybt0Mr1Dg">www.sissyresidence.com</a>, owned and operated by Sissy Residence. This privacy statement describes how we collect and use the personal information you provide on our website:&nbsp;<a href="http://www.sissyresidence.com/" target="_blank" rel="noopener" data-saferedirecturl="https://www.google.com/url?q=http://www.sissyresidence.com&amp;source=gmail&amp;ust=1518251651060000&amp;usg=AFQjCNGDyjt0mtuYNU2ze2mr3Ybt0Mr1Dg">www.sissyresidence.com</a>. It also describes the choices available to you regarding our use of your personal information and how you can access and update this information. We might amend this privacy statement from time to time, so visit this page regularly to stay up to date.</p>
                <h3>The type of personal information we collect.</h3>
                <p>The types of personal information that we collect include:</p>
                <ul>
                    <li>Your first name, last name, email address, phone number, and home address;</li>
                    <li>Credit card details (type of card, credit card number, name on card, expiration date, and security code);</li>
                    <li>Guest stay information, including date of arrival and departure, special requests made, observations about your service preferences (including room preferences, facilities or any other services used);</li>
                    <li>Information you provide regarding your marketing preferences or in the course of participating in surveys, contests or promotional offers;</li>
                </ul>
                <p>When you visit our website, even if you do not make a reservation, we may collect certain information, such as your IP address, which browser you&rsquo;re using, and information about your computer&rsquo;s operating system, application version, language settings and pages that have been shown to you. If you are using a mobile device, we may also collect data that identifies your mobile device, device-specific settings and characteristics and latitude/longitude details. When you make a reservation, our system registers through which means and from which websites you have made your reservation. If this data can identify you as a natural person, this data is considered personal information which is subject to this privacy statement.</p>
                <p>You can always choose what personal information (if any) you want to provide to us. If you decide not to provide certain details, however, some of your transactions with us may be impacted.</p>
                <h3>Why do we collect, use and share your personal information?</h3>
                <ul>
                    <li>Reservations: We use your personal information to complete and manage your online reservation.</li>
                    <li>Customer Service: We use your personal information to provide customer service.&nbsp;</li>
                    <li>Guest Reviews: We may use your contact information to invite you by email to write a guest review after your stay. This can help other travelers to select the accommodations that fit them best.</li>
                    <li>Marketing Activities: We also use your information for marketing activities, as permitted by law.&nbsp;</li>
                    <li>Other communications: There may be other times when we get in touch by email, mail, phone or by texting you, depending on the contact information you share with us. There could be a number of reasons for this:
                        <ul>
                            <li>We may need to respond to and handle requests you have made.</li>
                            <li>If you have not finalized a reservation online, we may email you a reminder to continue with your reservation. We believe that this additional service is useful to you because it allows you to carry on with a reservation without having to search for the accommodations again or fill in all the reservation details from scratch.</li>
                            <li>When you use our services, we may send you a questionnaire or invite you to provide a review of your experience with our website.</li>
                        </ul>
                    </li>
                    <li>Legal Purposes: In certain cases, we may need to use your information to handle and resolve legal disputes, for regulatory investigations and compliance.</li>
                    <li>Fraud Detection and Prevention: We may use your personal information for the detection and prevention of fraud and other illegal or unwanted activities.</li>
                    <li>Improving Our Services: Finally, we use your personal information for analytical purposes, to improve our services, to enhance the user experience, and to improve the functionality and quality of our online travel services.</li>
                </ul>
                <h3>To process your information as described above, we rely on the following legal basis:</h3>
                <ul>
                    <li>Performance of a Contract: The use of your information may be necessary to perform the contract that you have with us. For example, if you use our services to make an online reservation, we'll use your information to carry out our obligation to complete and manage that reservation under the contract that we have with you.</li>
                    <li>Legitimate Interests: We may use your information for our legitimate interests, such as providing you with the best appropriate content for the website, emails and newsletters, to improve and promote our products and services and the content on our website, and for administrative, fraud detection and legal purposes.</li>
                    <li>Consent: We may rely on your consent to use your personal information for certain direct marketing purposes. You can withdraw your consent anytime by contacting us at any of the addresses at the end of this Privacy Statement.</li>
                </ul>
                <ul>
                    <li>Third-party Service Providers: We may use service providers to process your personal information strictly on our behalf. This processing would be for purposes such as facilitating reservation payments, sending out marketing material or for analytical support services. These processors are bound by confidentiality clauses and are not allowed to use your personal data for their own purposes or any other purpose.</li>
                    <li>Competent Authorities: We disclose personal information to law enforcement and other governmental authorities insofar as it is required by law or is strictly necessary for the prevention, detection or prosecution of criminal acts and fraud.</li>
                </ul>
                <h3>Retention of Personal Information</h3>
                <p>We will retain your personal information for as long as we deem it necessary to provide services to you, comply with applicable laws (including those regarding document retention), resolve disputes with any parties and otherwise as necessary to allow us to conduct our business. All personal information we retain will be subject to this privacy statement.</p>
                <h3>What security procedures are put in place to safeguard your personal data?</h3>
                <p>In accordance with applicable data protection laws, we observe reasonable procedures to prevent unauthorized access to, and the misuse of, personal data. We use appropriate business systems and procedures to protect and safeguard the personal data you give us. We also use security procedures and technical and physical restrictions for accessing and using the personal data on our servers. Only authorized personnel are permitted to access personal data in the course of their work.</p>
                <h3>How can you control your personal information?</h3>
                <p>You always have the right to review the personal information we keep about you. You can request an overview of your personal data by emailing us at the email address stated below. Include "Request personal information" in the subject line of your email to speed things up.&nbsp;<br />You can also contact us if you believe that the personal information we have for you is incorrect, if you believe that we are no longer entitled to use your personal data, or if you have any other questions about how your personal information is used or about this Privacy Statement. Email or write to us using the contact details below. We will handle your request in accordance with the applicable data protection law.</p>
                <h3>Who is responsible for the processing of your personal data?</h3>
                <p>Sissy Residence,&nbsp;<a href="https://maps.google.com/?q=1073+Budapest,+Erzsebet+Krt.+24,+Hungary&amp;entry=gmail&amp;source=g" data-saferedirecturl="https://www.google.com/url?q=https://maps.google.com/?q%3D1073%2BBudapest,%2BErzsebet%2BKrt.%2B24,%2BHungary%26entry%3Dgmail%26source%3Dg&amp;source=gmail&amp;ust=1518251651061000&amp;usg=AFQjCNFDqBaY91QbAUN8bgS9cNZAbOeVuQ">1073 Budapest, Erzsebet Krt. 24, Hungary</a>. If you have any suggestions or comments about this privacy statement, send a message via the content form.</p>
                <p><!--StartFragment--></p>
                <h3>Please read this policy carefully because by staying here you automatically accept these terms:</h3>
                <ul>
                    <li>All rooms and whole residence is NO SMOKING place. Please do not smoke inside otherwise</li>
                    <li>a 100 EUR fee applies.</li>
                    <li>If you loose the key, the fine is 200&euro;! For any damages, fine starts at 100&euro;.</li>
                    <li>You have acces to FREE WIFI&nbsp;</li>
                    <li>We are a water and natural friendly residence. We do not change towels every day however</li>
                    <li>if you need a new one please drop the used one on the floor in the bathroom and we will</li>
                    <li>provide you a clean one.</li>
                    <li>Our reception is open at 7:00 and closes 20:00 in the evening. During the operational hours,</li>
                    <li>we are glad to help you in everything you need.</li>
                    <li>If you come home late in the night please respect other guest sleep and do not make loud</li>
                    <li>noises.</li>
                    <li>We do not take any responsibility for goods left in the room, so please always have your</li>
                    <li>wallett, passport with you. We have CCTV in operation in common areas of this residence.</li>
                    <li>Breakfast time: 7:30-10:00.</li>
                </ul>
            </div>
        </div>
    </div>
    {{-- /Privacy policy --}}

@endsection

@section('js_body')
    @parent

@endsection