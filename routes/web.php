<?php

// Homepage
Route::get('/', 'DefaultController@homepage')->name('homepage');

// Privacy policy
Route::get('/privacy-policy', 'DefaultController@privacyPolicyIndex')->name('privacy_policy');

// Send contact form
Route::post('/send-contact-form', 'ContactController@sendContactForm')->name('send_contact_form');