<?php

namespace App\Http\Controllers;

use Mail;
use Cache;
use App\Mail\ContactMail;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Submit contact form
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendContactForm(Request $request)
    {
        $routeParams = [];

        if($request->has('submit_contact')){

            // Get data
            $data = $request->only(['first_name', 'last_name', 'email', 'phone', 'comment']);

            Mail::to(env('MAIL_ADMIN_ADDRESS'))->send(new ContactMail($data));

            // Cache contact-sent status
            Cache::remember('contact_sent', 10000, function(){
               return true;
            });
        }

        return redirect(route('homepage') . '#contact');
    }
}
