<?php

namespace App\Http\Controllers;

use Log;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefaultController extends Controller
{

    /**
     * Homepage
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function homepage(Request $request)
    {
        return view('homepage.index');
    }


    /**
     * Privacy policy index
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacyPolicyIndex(Request $request)
    {
        return view('privacy-policy.index');
    }
}
